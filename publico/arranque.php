<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

try {
    // Iniciamos el uso del registrador
    openlog('nb_api', LOG_CONS | LOG_PERROR, LOG_USER);

    function gestor_error_excepcion($errno, $errstr, $errfile, $errline)
    {
        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
    }

    set_error_handler("gestor_error_excepcion");

    function gestor_error_fatal()
    {
        $error = error_get_last();
        if (isset($error)) {
            throw new ErrorException($error['message'], 0, $error['type'], $error['file'], $error['line']);
        }
    }

    register_shutdown_function('gestor_error_fatal');


    // Preparamos auto-cargadores de clases
    // * Requerido
    require '../NP/Cargador.php';
    $cargador = new \NP\Cargador();
    $cargador->registrarDir('../app/controladores');
    $cargador->registrarDir('../app/modelos');
    $cargador->registrar();

    // Iniciamos contenedor de servicios, componentes y configuraciones
    // * Requerido
    $servicios = \NP\Servicios::instanciar();

    // Cargamos las configuraciones globales
    // * Requerido
    $servicios['config'] = require '../app/config/global.php';

    // Preparamos servicio de vistas
    // * Requerido
    $servicios['vista'] = function () use ($servicios) {
        $vista = new \NP\Vista($servicios);
        $vista->plantilla = null;
        return $vista;
    };

    // Preparamos servicio de cache
    $servicios['cache'] = function () use ($servicios) {
        return new \NP\Cache\Memcache([
            'servidores' => [
                ['localhost', 11211, 1],
            ]
        ]);
    };

    // Preparamos servicio de base de datos sql
    $servicios['datos'] = function () use ($servicios) {
        return new \NP\Datos([
            'servidores' => [
                ['mysql:host=maria01;dbname=nearbooking', 1]
            ],
            'usuario' => 'nb_api',
            'contrasena' => 'dpQBlgWWBlSUjsqOOwsv',
        ]);
    };

    // Preparamos servicio de peticion
    $servicios['peticion'] = function () {
        return new \NP\Peticion();
    };

    // Preparamos y corremos aplicacion
    $app = new \NP\Aplicacion($servicios);
    $app->ejecutar();

} catch (\NP\ExcepcionPublica $e) {

    http_response_code(500);
    echo json_encode([
        'error' => 'Error interno de servidor',
        'mensaje' => $e->getMessage(),
        'excepcion' => $e->getDetalle(),
    ]);

} catch (Exception $e) {

//    header('HTTP/1.1 500 Internal Server Error', true, 500);
    http_response_code(500);
    echo json_encode([
        'error' => 'Error interno de servidor',
        'excepcion' => $e,
    ]);
//    syslog(LOG_ALERT, "[$tipo] $mensaje dentro de $archivo en linea $linea");
    exit(1);

}
