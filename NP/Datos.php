<?php

namespace NP;

/**
 * Gestor de Base de datos SQL que extiende PDO
 *
 * @author Néstor Picado <info@nestorpicado.com>
 * @package NP
 * @version 0.4 (2015-03-13)
 */
class Datos
{
    /** @var \PDO */
    public $pdo = null;

    /**
     * @var \PDOStatement
     */
    public $sentencia;
    public $sentencia_params;
    public $sentencia_cruda;

    /** @var Cache\AdaptadorInterface */
    public $cache;
    private $cache_ttl;     // segundos
    private $cache_llave ;  // cadena id de llave
    private $cache_activar = true;
    private $cache_sobrescribir = false;
    private $cache_auto;

    private $depurar = false;

    /**
     * Constructor con configuraciones
     *
     * @param array $config
     * @throws \InvalidArgumentException  si las configuraciones son incorrectas
     * @throws \Exception  si ocurre un problema de conexion
     */
    public function __construct(array $config)
    {
        // Verificamos configuraciones requeridas
        if (!isset($config['usuario'], $config['contrasena'], $config['servidores'][0][0], $config['servidores'][0][1])) {
            throw new \InvalidArgumentException('Faltan configuraciones requeridas o algunas son inválidas.', 4006);
        }

        if (isset($config['auto_cache'])) {
            $this->cache_auto = $config['auto_cache'];
        }

        if (!empty($config['depurar'])) {
            $this->depurar = true;
        }

        // Calculamos orden de conexiones segun peso
        $pesos = array();
        foreach ($config['servidores'] as $llave => $valor) {
            $pesos[$llave] = $valor[1];
        }
        $pesos_total = array_sum($pesos);
        foreach ($pesos as $llave => $valor) {
            $pesos[$llave] = ($valor / $pesos_total) * mt_rand(1, 5);
        }
        arsort($pesos);
        $pesos = array_keys($pesos);

        // Nos conectamos al servidor disponible segun orden
        $conectado = false;
        while (!$conectado && count($pesos)) {
            $llave = array_shift($pesos);
            try {
                $this->pdo = @new \PDO(
                    $config['servidores'][$llave][0],
                    $config['usuario'],
                    $config['contrasena'],
                    array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION)
                );
                $conectado = true;
            } catch (\PDOException $e) {
                if ($e->getCode() == 1045) {
                    throw new \Exception('Acceso denegado a la base de datos.', 4002);
                } else {
                    //var_export($e);
                    // TODO Registrar errores de conexión
                }
                $conectado = false;
            }
        }

        if ($conectado) {
            $this->log('Abrimos conexion a la de base de datos.');

            // Definimos el conjunto de caracteres
            if (isset($config['charset'])) {
                $this->pdo->exec('SET CHARACTER SET ' . $config['charset']);
            }
        } else {
            throw new \Exception('No se pudo abrir conexion a la base de datos.', 4005);
        }

    }

    /**
     * @return void
     */
    function __destruct()
    {
        $this->desconectar();
    }

    /**
     * Registra un mensaje visualmente detallado para depurar el gestor
     *
     * @param string $mensaje
     * @param string $detalle
     */
    private function log($mensaje, $detalle = null)
    {
        if ($this->depurar) {
            echo '<div style="font-weight: bold; font-family: verdana, arial, helvetica, sans-serif; '
                . 'font-size: 13px; line-height: 16px; color: #000; background-color: #E6E6FF; border: solid 1px #99F; '
                . 'padding: 4px 6px; margin: 10px; position: relative;">' . $mensaje . "<br>"
                . '<pre style="font-weight: normal; font-family: monospace; font-size: 12px;">'
                . print_r($detalle) . '</pre></div>';
        }
    }

    /**
     * Cierra la conexión al servidor
     *
     * @return bool
     */
    public function desconectar()
    {
        $this->sentencia = null;
        $this->pdo = null;
        return true;
    }

    /**
     * Selecciona la base de datos interna
     *
     * @param string $bd
     * @return Datos
     * @throws \Exception
     */
    public function seleccionarBd($bd)
    {
        if ($this->pdo->exec('USE ' . $bd) !== false) {
            return $this;
        } else {
            throw new \Exception('No se pudo seleccionar base de datos.', 4004);
        }
    }

    /**
     * Prepara valor segun tipo especificado
     *
     * @param mixed $valor Valor a preparar
     * @param string $tipo Tipo de valor pasado: bol, txt, num, def, auto
     * @return string Retorna valor escapado para MySQL
     */
    public function prepararValor($valor, $tipo = 'auto')
    {
        if (is_array($valor)) {

            if ([] === $valor) return 'NULL';

            foreach ($valor as $llave => $v) {
                $valor[$llave] = $this->prepararValor($v, $tipo);
            }

            return $valor;

        } else {

            if ('auto' == $tipo) {
                if (is_numeric($valor)) {
                    $tipo = 'num';
                } else {
                    $tipo = 'txt';
                }
            }

            // Retornamos valor boleano
            if ($tipo == 'bol') {
                return (empty($valor)) ? 0 : 1;
            }

            // Retornamos valor nulo
            if ($valor === null || $valor === false) {
                return 'NULL';
            }

            // Retornamos valor textual
            if ($tipo == 'txt') {
                return $this->pdo->quote($valor);
            }

            // Retornamos valor numerico
            if ($tipo == 'num') {
                if ($valor === '') return 'NULL';

                return floatval($valor);
            }

            return $valor;
        }
    }

    /**
     * Introduce directamente la consulta preparada
     *
     * @param string $sentencia
     * @param array|null $params
     * @return Datos
     * @throws \Exception si la sentencia es invalida
     */
    public function sql($sentencia, array $params = null)
    {
        if ($this->sentencia = $this->pdo->prepare($sentencia)) {
            if (isset($params)) $this->sentencia_params = $params;
            $this->sentencia_cruda = $sentencia;
        } else {
            throw new \Exception('La sentencia introducida es inválida.');
        }

        return $this;
    }

    /**
     * Obtiene multiples registros
     *
     * @param string $indizar_por Campo que desea como llaves de arreglo
     * @param string $agrupar_por Campo que desea agrupar
     * @return array Retorna un arreglo con o sin resultados
     * @throws \Exception
     */
    public function obtener($indizar_por = null, $agrupar_por = null)
    {
        // Validamos la sentencia a ejecutar
        if (!isset($this->sentencia)) {
            if (isset($this->sentencia_cruda)) {
                $this->sql($this->sentencia_cruda);
            } else {
                throw new \Exception('La sentencia a ejecutar esta vacia.');
            }
        }

        // Se obtiene resultados desde cache según el caso
        $temp = $this->obtenerCache($this->sentencia_cruda . serialize($this->sentencia_params) . $indizar_por . $agrupar_por);
        if ($temp !== false) return $temp;

        // Se obtiene resultados desde BD
        $ti = microtime(true);
        if ($this->sentencia->execute($this->sentencia_params)) {

            $final = array();
            $fila = $this->sentencia->fetch(\PDO::FETCH_ASSOC);

            if (isset($agrupar_por) && isset($fila[$agrupar_por])) {

                if (isset($indizar_por) && isset($fila[$indizar_por])) {
                    while ($fila) {
                        $final[$fila[$agrupar_por]][$fila[$indizar_por]] = $fila;
                        $fila = $this->sentencia->fetch(\PDO::FETCH_ASSOC);
                    }
                } else {
                    while ($fila) {
                        $final[$fila[$agrupar_por]][] = $fila;
                        $fila = $this->sentencia->fetch(\PDO::FETCH_ASSOC);
                    }
                }

            } else {

                if (isset($indizar_por) && isset($fila[$indizar_por])) {
                    while ($fila) {
                        $final[$fila[$indizar_por]] = $fila;
                        $fila = $this->sentencia->fetch(\PDO::FETCH_ASSOC);
                    }
                } else {
                    while ($fila) {
                        $final[] = $fila;
                        $fila = $this->sentencia->fetch(\PDO::FETCH_ASSOC);
                    }
                }

            }

            $this->sentencia->closeCursor();

            $this->log('Obtenemos multiples registros desde BD en ' . round((microtime(true) - $ti) * 1000, 3)
                . ' ms. [indice: ' . $indizar_por . ', grupo: ' . $agrupar_por . ']', $this->sentencia->queryString);

            // Se guarda resultados en cache según el caso
            $this->guardarCache($final);

            return $final;

        } else {
            throw new \Exception('La sentencia [ ' . $this->sentencia->queryString . ' ] dió el siguiente error: '
                . $this->sentencia->errorInfo() . '.', 102);
        }
    }

    /**
     * Ejecuta una sentencia SQL
     *
     * @param string $params Sentencia SQL a ejecutar
     * @throws \Exception
     * @return boolean|int
     */
    public function ejecutar($params = null)
    {
        // Validamos la sentencia a ejecutar
        if (!isset($this->sentencia)) {
            if (isset($this->sentencia_cruda)) {
                $this->sql($this->sentencia_cruda);
            } else {
                throw new \Exception('La sentencia a ejecutar esta vacia.');
            }
        }

        // Ejecutamos la sentencia con los debidos parámetros
        $ti = microtime(true);
        if (!isset($params)) {
            $params = $this->sentencia_params;
        }
        if ($this->sentencia->execute($params)) {
            $this->log('Ejecutamos desde BD la siguiente consulta (' . round((microtime(true) - $ti) * 1000, 3) . ' ms.) :' .
                '<pre style="font-weight: normal; font-family: monospace; font-size: 12px;">' . $this->sentencia->queryString . '</pre>');

            if (stripos($this->sentencia->queryString, 'INSERT INTO ') !== false) {
                return $this->pdo->lastInsertId();
            } else {
                return true;
            }
        } else {
            throw new \Exception('La sentencia [ ' . $this->sentencia->queryString . ' ] dió el siguiente error: ' . $this->sentencia->errorInfo() . '.', 103);
        }
    }

    /**
     * Obtiene solo un registro como vector
     *
     * @return array|boolean Retorna un vector si hay resultados o falso sino hay
     * @throws \Exception
     */
    public function obtenerFila()
    {
        // Validamos la sentencia a ejecutar
        if (!isset($this->sentencia)) {
            if (isset($this->sentencia_cruda)) {
                $this->sql($this->sentencia_cruda);
            } else {
                throw new \Exception('La sentencia a ejecutar esta vacia.');
            }
        }

        // Se obtiene resultados desde cache según el caso
        $temp = $this->obtenerCache($this->sentencia_cruda . serialize($this->sentencia_params) . 'fila');
        if ($temp !== false) return $temp;

        // Se obtiene resultados desde MySQL
        $ti = microtime(true);
        if ($this->sentencia->execute($this->sentencia_params)) {
            $final = $this->sentencia->fetch(\PDO::FETCH_ASSOC);

            $this->log('Obtenemos solo un registro desde BD en' . round((microtime(true) - $ti) * 1000, 3) . ' ms.',
                $this->sentencia->queryString);

            // Se guarda resultados en cache en caso solicitado
            $this->guardarCache($final);

            return $final;
        } else {
            throw new \Exception('La consulta [ ' . $this->sentencia->queryString . ' ] dió el siguiente error: '
                . $this->sentencia->errorInfo() . '.', 102);
        }
    }

    /**
     * Prepara una sentencia SQL para su ejecución directa
     *
     * @param string $sentencia Sentencia SQL
     * @throws \Exception
     * @return \PDOStatement
     */
    public function preparar($sentencia = null)
    {
        if (!$sentencia) {
            throw new \Exception('La sentencia SQL a ejecutar esta vacia.');
        }

        return $this->pdo->prepare($sentencia);
    }

    /**
     * Obtiene el id incremental generado por la ultima ejecucion
     *
     * @return int|boolean
     */
    public function ultimoId()
    {
        return $this->pdo->lastInsertId();
    }


    // METODOS SOBRE CACHE

    /**
     * Implementa cache a la proxima consulta
     *
     * @param int $ttl Tiempo de vida en segundos
     * @param string|null $llave
     * @param bool $sobrescribir
     * @throws \Exception si el gestor de cache no fue registrado
     * @return Datos
     */
    public function usarCache($ttl, $llave = null, $sobrescribir = false)
    {
        if (!isset($this->cache)) {
            throw new \Exception('El gestor de cache no fue registrado.');
        }

        if ($ttl) {
            $this->cache_ttl = $ttl;
            $this->cache_llave = $llave;
            $this->cache_activar = true;
        } else {
            $this->cache_ttl = null;
            $this->cache_llave = null;
            $this->cache_activar = false;
        }
        $this->cache_sobrescribir = $sobrescribir;

        return $this;
    }

    /**
     * Obtiene datos directamente desde cache y prepara banderas internas
     *
     * @param string $llave_alterna
     * @throws \Exception
     * @return mixed
     */
    private function obtenerCache($llave_alterna)
    {
        if (($this->cache_ttl || $this->cache_auto) && $this->cache_activar) {
            if (!isset($this->cache)) {
                throw new \Exception('El gestor de cache no fue registrado.');
            }

            if (empty($this->cache_llave)) {
                $this->cache_llave = md5($llave_alterna);
            }

            if (!$this->cache_sobrescribir) {
                if ($this->cache->existe($this->cache_llave)) {
                    $this->log('Obtenemos datos desde cache "' . $this->cache_llave . '" con expiraci&oacute;n de ' .
                        ((empty($this->cache_ttl)) ? $this->c['auto_cache'] : $this->cache_ttl) . ' seg.');
                    $this->cache_ttl = null;
                    $this->cache_llave = null;
                    $this->cache_activar = true;
                    $this->cache_sobrescribir = false;
                    return $this->cache->obtener($this->cache_llave);
                }
            }
        }
        return false;
    }

    /**
     * Guarda resultados al cache segun banderas internas
     *
     * @param mixed $datos
     * @throws \Exception
     * @return void
     */
    private function guardarCache(&$datos)
    {
        if ($this->cache_ttl && $this->cache_activar) {
            if (!isset($this->cache)) {
                throw new \Exception('GestorMySQL | El gestor de cache no fue registrado.');
            }

            $this->cache->guardar($this->cache_llave, $datos, $this->cache_ttl);
            $this->log('Guardamos resultados en cache "' .
                $this->cache_llave . '" por ' . $this->cache_ttl . ' seg.');

        } elseif ($this->cache_auto && $this->cache_activar) {
            if (!isset($this->cache)) {
                throw new \Exception('GestorMySQL | El gestor de cache no fue registrado.');
            }

            $this->cache->guardar($this->cache_llave, $datos, $this->cache_auto);
            $this->log('Guardamos resultados automaticamente en cache "' .
                $this->cache_llave . '" por ' . $this->cache_auto . ' seg.');
        }
        $this->cache_ttl = null;
        $this->cache_llave = null;
        $this->cache_activar = true;
        $this->cache_sobrescribir = false;
    }


    // METODOS SOBRE TRANSACCIONES

    /**
     * Inicia una transaccion
     *
     * @return Datos
     */
    public function iniciarTransaccion()
    {
        $this->pdo->beginTransaction();
        return $this;
    }

    /**
     * Cierra transaccion activa
     *
     * @return Datos
     */
    public function cerrarTransaccion()
    {
        $this->pdo->commit();
        return $this;
    }

    /**
     * Revierte transaccion activa
     *
     * @return Datos
     */
    public function cancelarTransaccion()
    {
        $this->pdo->rollback();
        return $this;
    }


    // METODOS CRUD

    /**
     * Inicia la consulta preparada con SELECT
     *
     * @param string|array $campos
     * @param string|null $tabla
     * @return Datos
     */
    public function seleccionar($campos, $tabla = null)
    {
        $this->sentencia = null;
        $this->sentencia_params = null;
        if (is_array($campos)) {
            $this->sentencia_cruda = 'SELECT ' . implode(', ', $campos);
        } else {
            $this->sentencia_cruda = 'SELECT ' . $campos;
        }
        if ($tabla) $this->sentencia_cruda .= ' FROM ' . $tabla;

        return $this;
    }

    /**
     * Implementa FROM a la consulta preparada
     *
     * @param string $tabla
     * @return Datos
     */
    public function de($tabla)
    {
        $this->sentencia_cruda .= ' FROM ' . $tabla;

        return $this;
    }

    /**
     * Implementa JOIN a la consulta preparada
     *
     * @param string $tabla
     * @param string $campo1
     * @param string $campo2
     * @param string $modo
     * @return Datos
     */
    public function unirTabla($tabla, $campo1, $campo2, $modo = 'INNER')
    {
        $this->sentencia_cruda .= " $modo JOIN $tabla ON $campo1 = $campo2";

        return $this;
    }

    /**
     * Prepara los campos que filtran la consulta preparada
     *
     * @param array $params
     * @return Datos
     */
    public function donde(array $params)
    {
        $terminos_sql = array();

        foreach ($params as $llave => $valor) {
            // Detectamos y preparamos las variables campo, operador y tipo
            $llave = trim($llave);
            if (false === ($temp = strpos($llave, ' '))) {
                $campo = $llave;
            } else {
                $campo = substr($llave, 0, $temp);
                $operador = substr($llave, $temp + 1);
            }
            if (empty($operador)) $operador = '=';

            if (false !== ($temp = strpos($campo, '|'))) {
                $tipo = substr($campo, $temp + 1);
                $campo = substr($campo, 0, $temp);
            }
            if (empty($tipo)) $tipo = 'auto';

            // Preparamos el valor segun su tipo
            if (is_array($valor)) {
                $valor_preparado = '(' . implode(',', $this->prepararValor($valor, $tipo)) . ')';
            } else {
                $valor_preparado = $this->prepararValor($valor, $tipo);
            }

            // Escribimos el termino dentro de los filtros
            $terminos_sql[] = "$campo $operador $valor_preparado";

            // Limpiamos las variables repetitivas
            unset($campo, $operador, $tipo);
        }

        // Escribimos todos los terminos en formato SQL
        $this->sentencia_cruda .= ' WHERE ' . implode(' AND ', $terminos_sql);

        return $this;
    }

    /**
     * Implementa ORDER BY a la consulta preparada
     *
     * @param string $orden
     * @return Datos
     */
    public function ordenarPor($orden)
    {
        $this->sentencia_cruda .= ' ORDER BY ' . trim($orden);

        return $this;
    }

    /**
     * Implementa GROUP BY a la consulta preparada
     *
     * @param string $grupo
     * @return Datos
     */
    public function agruparPor($grupo)
    {
        $this->sentencia_cruda .= ' GROUP BY ' . trim($grupo);

        return $this;
    }

    /**
     * Implementa LIMIT a la consulta preparada
     *
     * @param int $pos
     * @param int $limite
     * @return Datos
     */
    public function limitar($pos, $limite = null)
    {
        if ($limite) {
            $this->sentencia_cruda .= ' LIMIT ' . $pos . ',' . $limite;
        } else {
            $this->sentencia_cruda .= ' LIMIT ' . $pos;
        }

        return $this;
    }

    /**
     * Inicia consulta preparada con UPDATE
     *
     * @param string $tabla
     * @param array $params
     * @return Datos
     */
    public function actualizar($tabla, array $params)
    {
        $this->sentencia = null;
        $this->sentencia_params = array();

        $terminos_sql = array();
        foreach ($params as $campo => $valor) {
            $terminos_sql[] = '`' . $campo . '` = :' . $campo;
            $this->sentencia_params[':' . $campo] = $valor;
        }

        $this->sentencia_cruda = 'UPDATE ' . $tabla . ' SET ' . implode(', ', $terminos_sql);

        return $this;
    }

    /**
     * Inicia consulta preparada con INSERT
     *
     * @param string $tabla Tabla
     * @param array $params Campos parametrizados
     * @return Datos
     */
    public function insertar($tabla, array $params)
    {
        $this->sentencia = null;
        $this->sentencia_params = array();
        $columnas = array();
        $valores = array();

        foreach ($params as $campo => $valor) {
            $columnas[] = '`' . $campo . '`';
            $valores[] = ':' . $campo;
            $this->sentencia_params[':' . $campo] = $valor;
        }

        $this->sentencia_cruda = 'INSERT INTO ' . $tabla . ' (' . implode(', ', $columnas) . ') VALUES (' . implode(', ', $valores) . ')';

        return $this;
    }

    /**
     * Inicia consulta preparada con DELETE FROM
     *
     * @param string $tabla
     * @return Datos
     */
    public function eliminar($tabla)
    {
        $this->sentencia = null;
        $this->sentencia_cruda = 'DELETE FROM ' . $tabla;

        return $this;
    }

    /**
     * Devuelve la consulta preparada hasta el momento
     *
     * @param bool $formato_html
     * @return string
     */
    public function obtenerSql($formato_html = false)
    {
        if ($formato_html) {
            return '<div style="font-weight: bold; font-family: verdana, arial, helvetica, sans-serif; '
            . 'font-size: 13px; line-height: 16px; color: #000; background-color: #E6E6FF; border: solid 1px #99F; '
            . 'padding: 4px 6px; margin: 10px; position: relative;">' . $this->sentencia_cruda . '</div>';
        } else {
            return $this->sentencia_cruda;
        }
    }
}
