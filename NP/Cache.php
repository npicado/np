<?php

namespace NP;

/**
 * Capa de Cache
 *
 * @author Néstor Picado <info@nestorpicado.com>
 * @package NP
 * @version 0.1 (2014-05-06)
 */
class Cache
{
    /**
     * @var Servicios
     */
    private $contenedor;
    /**
     * @var Cache\AdaptadorInterface
     */
    private $adaptador;

    public function __construct(Servicios $c, Cache\AdaptadorInterface $a)
    {
        $this->contenedor = $c;
        $this->adaptador = $a;
    }

    public function prueba()
    {
        echo "Prueba\n";
    }
} 
