<?php

namespace NP;

/**
 * Excepción del Marco de Trabajo
 *
 * @author Néstor Picado <info@nestorpicado.com>
 * @package NP
 * @version 0.1 (2014-06-19)
 */
class ExcepcionPublica extends \Exception
{
    protected $message = 'Excepción desconocida';
    protected $detalle;

    public function __construct($mensaje, $detalle = null, $codigo = 0, \Exception $anterior = null)
    {
        $this->detalle = $detalle;
        parent::__construct($mensaje, $codigo, $anterior);
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    final public function getDetalle() {
        return $this->detalle;
    }
}
