<?php

namespace NP;

/**
 * Cargador automático de clases o librerías
 *
 * @author Néstor Picado <info@nestorpicado.com>
 * @package NP
 * @version 0.5 (2014-06-19)
 */
class Cargador
{
    public $directorios = ['.'];

    public function __construct()
    {
        array_unshift($this->directorios, realpath(__DIR__ . '/../'));
    }

    /**
     * Registra directorio de inclusion
     *
     * @param string $directorio
     */
    public function registrarDir($directorio)
    {
        if ($temp = realpath($directorio)) {
            array_unshift($this->directorios, $temp);
        }
    }

    /**
     * Carga la clase o interfaz
     *
     * @param string $nombre
     * @return bool
     * @throws \Exception
     */
    public function cargarClase($nombre)
    {
        $nombre = ltrim($nombre, '\\');
        $archivo  = '';
        if ($lastNsPos = strrpos($nombre, '\\')) {
            $namespace = substr($nombre, 0, $lastNsPos);
            $nombre = substr($nombre, $lastNsPos + 1);
            $archivo  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $archivo .= str_replace('_', DIRECTORY_SEPARATOR, $nombre) . '.php';

        foreach ($this->directorios as $dir) {
            $temp = $dir . DIRECTORY_SEPARATOR . $archivo;
            if (is_file($temp)) {
                include $temp;
                return true;
            }
        }

        include 'Excepcion.php';
        throw new Excepcion('La clase "' . $nombre . '" no fue encontrada en los directorios de inclusión.');
    }

    /**
     * Instala el cargador de clases en la pila de carga automática SPL.
     */
    public function registrar()
    {
        spl_autoload_register(array($this, 'cargarClase'));
    }

    /**
     * Desinstala el cargador de clases de la pila autocargador SPL.
     */
    public function quitar()
    {
        spl_autoload_unregister(array($this, 'cargarClase'));
    }
}
