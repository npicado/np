<?php

namespace NP;

/**
 * Contenedor de servicios que implementa la inyección de dependencia para
 * crear instancias de todos los componentes que usa la aplicacion hasta ser necesarios.
 *
 * @author Néstor Picado <info@nestorpicado.com>
 * @package NP
 * @version 0.3 (2014-06-22)
 */
class Servicios implements \ArrayAccess
{
    /**
     * @var Servicios
     */
    private static $instancia;

    private $valores = array();
    private $valores_crudos = array();
    private $llaves = array();
    private $activados = array();

    /**
     * Obtiene instancia global del Contenedor
     *
     * @return Servicios
     */
    public static function instanciar()
    {
        if (isset(self::$instancia)) {
            return self::$instancia;
        } else {
            return self::$instancia = new self();
        }
    }

    /**
     * Crea una instancia del contenedor.
     *
     * Servicios y parámetros se pueden pasar como argumento al constructor.
     *
     * @param array $valores Parámetros u servicios.
     */
    public function __construct(array $valores = array())
    {
        foreach ($valores as $llave => $valor) {
            $this->offsetSet($llave, $valor);
        }
    }

    /**
     * Registra un parámetro o un servicio.
     *
     * Los servicios deben ser definidos usando funciones anónimas.
     *
     * @param  string $id  Identificador único para el parámetro o servicio
     * @param  mixed $valor  El valor del parámetro o función anónima para registrar un servicio
     * @throws \RuntimeException  Previene el reemplazo de un servicio ya activado
     */
    public function offsetSet($id, $valor)
    {
        if (isset($this->activados[$id])) {
            throw new \RuntimeException(sprintf('No se puede reemplazar el servicio activado "%s".', $id));
        }

        $this->valores[$id] = $valor;
        $this->llaves[$id] = true;
    }

    /**
     * Obtiene un parámetro o un servicio.
     *
     * @param string $id  Identificador único para el parámetro o servicio
     * @return mixed  El valor del parámetro o de un servicio
     * @throws \InvalidArgumentException si el identificador no está registrado
     */
    public function offsetGet($id)
    {
        if (!isset($this->llaves[$id])) {
            throw new \InvalidArgumentException(sprintf('El identificador "%s" no está registrado.', $id));
        }

        if (
            isset($this->activados[$id])
            || !is_object($this->valores[$id])
            || !method_exists($this->valores[$id], '__invoke')
        ) {
            return $this->valores[$id];
        }

        $this->activados[$id] = true;
        $this->valores_crudos[$id] = $this->valores[$id];

        return $this->valores[$id] = $this->valores[$id]($this);
    }

    /**
     * Comprueba si un parámetro o un servicio está registrado.
     *
     * @param string $id  Identificador único para el parámetro o servicio
     * @return bool
     */
    public function offsetExists($id)
    {
        return isset($this->llaves[$id]);
    }

    /**
     * Elimina un parámetro o un servicio.
     *
     * @param string $id  Identificador único para el parámetro o servicio
     */
    public function offsetUnset($id)
    {
        if (isset($this->llaves[$id])) {
            unset($this->valores[$id], $this->activados[$id], $this->valores_crudos[$id], $this->llaves[$id]);
        }
    }

    /**
     * @return Cache
     */
    public function cache()
    {
        return $this->offsetGet('cache');
    }

    /**
     * @return Datos
     */
    public function bd()
    {
        return $this->offsetGet('datos');
    }

    /**
     * @return Datos
     */
    public function datos()
    {
        return $this->offsetGet('datos');
    }

    /**
     * @return Vista
     */
    public function vista()
    {
        return $this->offsetGet('vista');
    }

    /**
     * @return Sesion
     */
    public function sesion()
    {
        return $this->offsetGet('sesion');
    }
}
