<?php

namespace NP;

/**
 * Capa Modelo
 *
 * @author Néstor Picado <info@nestorpicado.com>
 * @package NP
 * @version 0.2 (2014-06-22)
 */
abstract class Modelo extends \stdClass
{
    protected $_tabla;
    protected $_campos;

    /**
     * Rellena los campos usando un arreglo.
     *
     * @param array $datos
     * @return $this
     */
    public function rellenar(array $datos)
    {
        foreach ($datos as $llave => $valor) {
            $this->$llave = $valor;
        }

        return $this;
    }

    /**
     * Filtra un registro del modelo usando las propiedades alteradas.
     *
     * @return bool
     * @throws Excepcion
     */
    public function obtener()
    {
        // Preparamos variables a usar
        $filtro = array();
        $campos_llaves_faltantes = array();

        // Hacemos recorrido de campos
        foreach ($this->_campos as $campo => $meta) {

            // Procesamos campo llave
            if (!empty($meta['llave'])) {
                if (!isset($this->$campo) || $this->$campo === '') {
                    $campos_llaves_faltantes[] = $campo;
                } else {
                    $filtro[$campo . ':' . $meta['tipo']] = $this->$campo;
                }
            }

        }

        // Validamos presencia de campos llaves en filtro
        if (!$filtro) {
            throw new Excepcion('Falta rellenar algún campo llave.', $campos_llaves_faltantes, 201);
        }

        $datos = Servicios::instanciar()->datos();

//        return $datos->seleccionar(implode(',', array_keys($this->_campos)), $this->_tabla)->donde($filtro)->obtenerSql();
        $res = $datos->seleccionar(implode(',', array_keys($this->_campos)), $this->_tabla)->donde($filtro)->obtenerFila();

        if ($res) {
            foreach ($res as $campo => $valor) {
                $this->$campo = $valor;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Filtra un registro del modelo usando las propiedades alteradas.
     *
     * @return bool
     * @throws Excepcion
     */
    public function buscar()
    {
        return $this->obtener();
    }

    /**
     * Inserta un registro del modelo usando las propiedades modificadas.
     *
     * @throws Excepcion
     */
    public function insertar()
    {
        // Preparamos variables a usar
        $campos = array();
        $campos_faltantes = array();

        // Hacemos recorrido de campos
        foreach ($this->_campos as $campo => $meta) {

            // Validamos requerimiento de campo
            if (!empty($meta['requerido']) && $meta['tipo'] != 'autonumerico' && (!isset($this->$campo) || $this->$campo === '')) {
                $campos_faltantes[] = $campo;
                continue;
            }

            // Preparamos campos a insertar
            if ($meta['tipo'] == 'autonumerico') {
                if (!empty($this->$campo)) {
                    $campos[$campo . ':' . $meta['tipo']] = $this->$campo;
                }
            } else {
                $campos[$campo . ':' . $meta['tipo']] = $this->$campo;
            }
        }

        // Validamos presencia de campos requeridos
        if ($campos_faltantes) {
            throw new ModeloExcepcion('Faltan campos requeridos.', $campos_faltantes, 202);
        }

        $datos = Servicios::instanciar()->datos();

//        return $this->id = $datos->insertar($this->_tabla, $campos)->obtenerSql();
        $this->id = $datos->insertar($this->_tabla, $campos)->ejecutar();

        if ($this->id) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Actualiza registro del modelo filtrando con llaves.
     *
     * @throws Excepcion
     */
    public function actualizar()
    {
        // Preparamos variables a usar
        $filtro = array();
        $campos = array();
        $campos_faltantes = array();
        $campos_llaves_faltantes = array();

        // Hacemos recorrido de campos
        foreach ($this->_campos as $campo => $meta) {

            // Procesamos campo llave
            if (!empty($meta['llave'])) {
                if (!isset($this->$campo) || $this->$campo === '') {
                    $campos_llaves_faltantes[] = $campo;
                } else {
                    $filtro[$campo . ':' . $meta['tipo']] = $this->$campo;
                }
            }

            // Validamos requerimiento de campo
            if (!empty($meta['requerido']) && (!isset($this->$campo) || $this->$campo === '')) {
                $campos_faltantes[] = $campo;
                continue;
            }

            // Preparamos campos a actualizar
            $campos[$campo . ':' . $meta['tipo']] = $this->$campo;
        }

        // Validamos presencia de campos llaves en filtro
        if (!$filtro) {
            throw new Excepcion('Falta rellenar algún campo llave.', $campos_llaves_faltantes, 201);
        }

        // Validamos presencia de campos requeridos
        if ($campos_faltantes) {
            throw new Excepcion('Faltan campos requeridos.',  $campos_faltantes, 202);
        }

        $datos = Servicios::instanciar()->datos();

//        return $datos->actualizar($this->_tabla, $campos)->donde($filtro)->obtenerSql();
        return $datos->actualizar($this->_tabla, $campos)->donde($filtro)->ejecutar();
    }

    /**
     * Elimina registros filtrando con propiedades alteradas.
     *
     * @throws Excepcion
     */
    public function eliminar()
    {
        // Preparamos variables a usar
        $filtro = array();
        $campos_llaves_faltantes = array();

        // Hacemos recorrido de campos
        foreach ($this->_campos as $campo => $meta) {

            // Procesamos campo llave
            if (!empty($meta['llave'])) {
                if (!isset($this->$campo) || $this->$campo === '') {
                    $campos_llaves_faltantes[] = $campo;
                } else {
                    $filtro[$campo . ':' . $meta['tipo']] = $this->$campo;
                }
            }

        }

        // Validamos presencia de campos llaves en filtro
        if (!$filtro) {
            throw new Excepcion('Falta rellenar algún campo llave.', $campos_llaves_faltantes, 201);
        }

        $datos = Servicios::instanciar()->datos();

//        return $datos->eliminar($this->_tabla)->donde($filtro)->obtenerSql();
        return $datos->eliminar($this->_tabla)->donde($filtro)->ejecutar();
    }

}
