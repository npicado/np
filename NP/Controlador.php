<?php

namespace NP;

/**
 * Capa Controlador
 *
 * @author Néstor Picado <info@nestorpicado.com>
 * @package NP
 * @version 0.4 (2015-03-12)
 */
class Controlador
{
    /**
     * @var Servicios
     */
    protected $s;
    public $parametros;

    public function __construct(Servicios $s)
    {
        $this->s = $s;
    }

    /**
     * @param string $controlador
     * @param string $accion
     * @return mixed
     */
    public function alIniciar($controlador, $accion)
    {}

    public function alTerminar(&$valor_devuelto)
    {}

    /**
     * @param string $servicio
     * @return Cache\AdaptadorInterface
     */
    public function cache($servicio = 'cache')
    {
        return $this->s[$servicio];
    }

    /**
     * @param string $servicio
     * @return Peticion
     */
    public function peticion($servicio = 'peticion')
    {
        return $this->s[$servicio];
    }

    /**
     * @param string $servicio
     * @return Vista
     */
    public function vista($servicio = 'vista')
    {
        return $this->s[$servicio];
    }

    /**
     * @param string $servicio
     * @return Datos
     */
    public function bd($servicio = 'datos')
    {
        return $this->s[$servicio];
    }

    /**
     * @param string $servicio
     * @return Sesion
     */
    public function sesion($servicio = 'sesion')
    {
        return $this->s[$servicio];
    }
}
