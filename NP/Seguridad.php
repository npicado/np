<?php

namespace NP;

/**
 * Utilidades de seguridad
 *
 * @author Néstor Picado <info@nestorpicado.com>
 * @package NP
 * @version 0.1 (2014-10-12)
 */
class Seguridad
{
    private static $mapaBase32 = 'abcdefghijklmnopqrstuvwxyz234567';

    /**
     * Genera hash de una contraseña
     *
     * @param string $contrasena
     * @return string
     */
    public static function hashContrasena($contrasena)
    {
        if (function_exists('password_hash')) {
            return password_hash($contrasena, PASSWORD_DEFAULT);
        } else if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
            $salt = str_replace('+', '.', base64_encode(mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)));
            return crypt($contrasena, '$2y$10$' . $salt . '$');
        } else {
            return crypt($contrasena);
        }
    }

    /**
     * Verifica contraseña contra su propio hash
     *
     * @param string $contrasena
     * @param string $hash
     * @return bool
     */
    public static function verificarContrasena($contrasena, $hash)
    {
        if (function_exists('password_verify')) {
            return password_verify($contrasena, $hash);
        } else {
            return crypt($contrasena, $hash) == $hash;
        }
    }

    public static function generarIV($usarBase64 = true)
    {
        $temp = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_RAND);

        if ($usarBase64) {
            return base64_encode($temp);
        } else {
            return $temp;
        }
    }

    public static function cifrar($valor, $llave, $iv, $usarBase64 = true)
    {
        if (base64_decode($iv, true)) {
            $iv = base64_decode($iv);
        }

        $temp = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $llave, $valor, MCRYPT_MODE_CBC, $iv);

        if ($usarBase64) {
            return base64_encode($temp);
        } else {
            return $temp;
        }
    }

    public static function decifrar($valor, $llave, $iv)
    {
        if (base64_decode($iv, true)) {
            $iv = base64_decode($iv);
        }
        if (base64_decode($valor, true) && preg_match('#^[a-zA-Z0-9+/]+={0,2}$#', $valor) ) {
            $valor = base64_decode($valor);
        }

        $valor = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $llave, $valor, MCRYPT_MODE_CBC, $iv);

        return rtrim($valor, "\0\3");
    }

    private function decodificarBase32($valor)
    {
        $l = strlen($valor);
        $n = $bs = 0;

        $salida = '';
        for ($i = 0; $i < $l; $i++) {
            $n <<= 5;
            $n += stripos(self::$mapaBase32, $valor[$i]);

            $bs = ( $bs + 5 ) % 8;
            $salida .= $bs < 5 ? chr( ($n & (255 << $bs)) >> $bs ) : null;
        }

        return $salida;
    }

    public function generarSecretoTOTP($longitud = 16)
    {
        if ($longitud < 16 || $longitud % 8 != 0) {
            throw new Excepcion('La longitud debe ser múltiple de 8 y ser mayor o igual a 16.');
        }

        $llave = '';
        while ($longitud--) {
            mt_srand();
            $llave .= self::$mapaBase32[mt_rand(0, 31)];
        }

        return strtoupper($llave);
    }

    public function generarCodigoTOTP($secreto, $digitos = 6, $periodo = 30)
    {
        if ( strlen($secreto) < 16 || strlen($secreto) % 8 != 0 )
            return [ 'err'=>'length of secret must be a multiple of 8, and at least 16 characters' ];
        if( $digitos < 6 || $digitos > 8 )
            return [ 'err'=>'digits must be 6, 7 or 8' ];

        $seed = self::decodificarBase32( $secreto );
        $time = intval( time() / $periodo );
        $time = str_pad( pack('N', $time), 8, "\x00", STR_PAD_LEFT );
        $hash = hash_hmac( 'sha1', $time, $seed, false );
        $otp = ( hexdec(substr($hash, hexdec($hash[39]) * 2, 8)) & 0x7fffffff ) % pow( 10, $digitos );

        return [ 'otp'=>sprintf("%'0{$digitos}u", $otp) ];
    }

    /**
     * Genera una URL que activa el autenticador TOTP
     *
     * @param $etiqueta
     * @param $secreto
     * @param int $digitos
     * @param int $periodo
     * @return string
     */
    function generarUrlTOTP($etiqueta, $secreto, $digitos = 6, $periodo = 30)
    {
        return 'otpauth://totp/' . rawurlencode($etiqueta) . "?secret=$secreto&digits=$digitos&period=$periodo";
    }

    /**
     * Devuelve una URL que genera una imagen con código QR
     *
     * @param string $url
     * @return string
     */
    public function generarUrlQRTOTP($url) {
        $urlencoded = urlencode($url);
        return 'https://chart.googleapis.com/chart?chs=200x200&chld=M|0&cht=qr&chl='.$urlencoded.'';
    }
}
