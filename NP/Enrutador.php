<?php

namespace NP;

/**
 * Enrutador de URI
 *
 * @package NP
 * @author Néstor Picado <info@nestorpicado.com>
 * @version 0.5 (2015-02-13)
 */
class Enrutador
{
    private function ubicar(&$rutas, $app_uri_base)
    {
        $peticion_metodo = strtolower($_SERVER['REQUEST_METHOD']);
        $peticion_uri = $_SERVER['REQUEST_URI'];

        // Retiramos los queries del URI
        if (($temp = strpos($peticion_uri, '?')) !== false) {
            $peticion_uri = substr($peticion_uri, 0, $temp);
        }

        // Retiramos la base del URI
        $peticion_uri = substr($peticion_uri, strlen($app_uri_base) - 1);
        if ($peticion_uri === false) {
            $peticion_uri = substr($peticion_uri . '/', strlen($app_uri_base) - 1);
            if ($peticion_uri === false) {
                $peticion_uri = '/';
            }
        }

        // Retiramos las barras adicionales encontradas al final del URI
        $peticion_uri = rtrim($peticion_uri, '/');
        if ($peticion_uri === '') $peticion_uri = '/';

        // Seleccionamos rutas posibles segun metodo de peticion
        if (!isset($rutas['*']) && !isset($rutas[$peticion_metodo])) {
            return [null, null];
        } else if (!isset($rutas['*']) && isset($rutas[$peticion_metodo])) {
            $rutas_posibles = $rutas[$peticion_metodo];
        } else if (isset($rutas['*']) && !isset($rutas[$peticion_metodo])) {
            $rutas_posibles = $rutas['*'];
        } else {
            $rutas_posibles = array_merge($rutas['*'], $rutas[$peticion_metodo]);
        }

        // Probamos ruteo directo evitando campos dinamicos
        if (strpos($peticion_uri, '{') === false) {
            if (isset($rutas_posibles[$peticion_uri])) {
                return [$rutas_posibles[$peticion_uri], null];
            } elseif (isset($rutas_posibles[$peticion_uri . '/'])) {
                return [$rutas_posibles[$peticion_uri . '/'], null];
            }
        }

        // Buscamos en todas las rutas posibles
        foreach ($rutas_posibles as $patron => $ruta) {
            if (strpos($patron, '{') !== false) {
                $patron = str_replace('{', '(?<', rtrim($patron, '/'));
//                $patron = str_replace('}', '>[\w\-\.]+)', $patron);
                $patron = str_replace('}', '>[^/]+)', $patron);
                $patron = '#^' . $patron . '$#';
//                echo $peticion_uri . ' > ' . $patron . "\n";
                if (preg_match($patron, $peticion_uri, $parametros)) {
                    unset($parametros[0]);
                    return [$ruta, $parametros];
                }
            }
        }

        return [null, null];
    }

    /**
     * Analiza el URI y trata de encontrar una ruta que coincida con los patrones
     *
     * @param array $rutas
     * @param string $app_uri_base Ruta relativa del subdirectorio donde se encuentra la aplicación. Ej. '/admin/'
     * @return array Devuelve un arreglo con la ruta encontrada
     */
    public function ejecutar(&$rutas, $app_uri_base = '/')
    {
        list($ruta, $parametros) = $this->ubicar($rutas, $app_uri_base);

        // Ruta no encontrada
        if (!$ruta) {
            return 404;
        }

        if (is_array($ruta)) {
            if ($ruta[0] == 'redir') {
                return $ruta;
            }

            if (isset($ruta[2])) {
                $parametros = array_merge((array)$parametros, $ruta[2]);
            }

            return [$ruta[0], $ruta[1], $parametros];
        } else {
            return $ruta;
        }
    }

}
