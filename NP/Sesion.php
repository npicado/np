<?php

namespace NP;

/**
 * Envoltura para sesion
 *
 * @author Néstor Picado <info@nestorpicado.com>
 * @package NP
 * @version 0.4 (2014-07-07)
 */
class Sesion
{
    private $conf = [
        'gestor' => 'files',
        'gestor_camino' => 'temp',
        'nombre' => 'NPSES',
        'tiempo_espera' => 0, // segundos
        'galleta_ttl' => 0, // segundos
        'galleta_camino' => '/',
        'galleta_dominio' => '',
        'galleta_seguro' => false,
        'galleta_solohttp' => true,
        'cache_limiter' => 'nocache',
        'cache_expira' => 180,
        'validar_ip' => false,
        'validar_agente' => false,
        'auto_regenerar' => false,
    ];

    /**
     * Configura detalles de sesion antes de iniciarla.
     *
     * @param array $conf
     */
    public function configurar(array $conf)
    {
        $this->conf = array_merge($this->conf, $conf);

        ini_set('session.hash_function', 'sha256');
        ini_set('session.entropy_file', '/dev/urandom');

        if ($this->conf['gestor'] == 'files' && $this->conf['gestor_camino'] == 'temp') {
            ini_set('session.save_handler', 'files');
            ini_set('session.save_path', sys_get_temp_dir());
        } else {
            ini_set('session.save_handler', $this->conf['gestor']);
            ini_set('session.save_path', $this->conf['gestor_camino']);
        }

        session_name($this->conf['nombre']);
        session_set_cookie_params(
            $this->conf['galleta_ttl'],
            $this->conf['galleta_camino'],
            $this->conf['galleta_dominio'],
            $this->conf['galleta_seguro'],
            $this->conf['galleta_solohttp']);
        session_cache_limiter($this->conf['cache_limiter']);
        session_cache_expire($this->conf['cache_expira']);
    }

    public function __construct(array $conf = null)
    {
        if (isset($conf)) $this->configurar($conf);
    }

    /**
     * Inicia la sesión si no se ha iniciado todavía.
     *
     * @throws Excepcion
     */
    public function iniciar()
    {
        if (session_start()) {

            // Auto regeneramos id segun caso
            if ($this->conf['auto_regenerar']) {
                session_regenerate_id(true);
            }

            // Validamos tiempo de espera en caso solicitado
            if ($this->conf['tiempo_espera']) {
                if (!isset($_SESSION['__tiempo_espera'])) {
                    $_SESSION['__tiempo_espera'] = time() + $this->conf['tiempo_espera'];
                } else {
                    if ($_SESSION['__tiempo_espera'] < time()) {
                        $this->destruir();
                        throw new Excepcion(
                            'Sesión Expirada',
                            'Lo sentimos pero ha excedido el tiempo de espera necesario para interactuar con el sistema, por favor cierre esta ventana y trate de nuevo.',
                            5001
                        );
                    } else {
                        $_SESSION['__tiempo_espera'] = time() + $this->conf['tiempo_espera'];
                    }
                }
            }

            // Validamos agente en caso solicitado
            if ($this->conf['validar_agente']) {
                if (isset($_SESSION['__agente'])) {
                    if ($_SESSION['__agente'] != sha1($_SERVER['HTTP_USER_AGENT'])) {
                        $this->destruir();
                        throw new Excepcion(
                            'Sesión Comprometida',
                            'Al parecer su sesión ha sido comprometida, por favor inicie de nuevo.',
                            5002
                        );
                    }
                } else {
                    $_SESSION['__agente'] = sha1($_SERVER['HTTP_USER_AGENT']);
                }
            }

            // Validamos IP en caso solicitado
            if ($this->conf['validar_ip']) {
                if (isset($_SESSION['__ip'])) {
                    if ($_SESSION['__ip'] !=$this->obtenerIp()) {
                        $this->destruir();
                        throw new Excepcion(
                            'Sesión Comprometida',
                            'Al parecer su sesión ha sido comprometida, por favor reinicie operaciones.',
                            5002
                        );
                    }
                } else {
                    $_SESSION['__ip'] = $_SERVER['REMOTE_ADDR'];
                }
            }

            // Registramos cierre de sesion al final
            register_shutdown_function('session_write_close');

        } else {

            throw new Excepcion(
                'Fallo de sesión',
                'Lamentablemente hemos detectado una condición inesperada que impide activar su sesión.',
                5000
            );

        }
    }

    /**
     * Termina la sesion actual y guarda los datos en ella.
     */
    public function cerrar()
    {
        if (session_status() === PHP_SESSION_ACTIVE) {
            session_write_close();
        }
    }

    /**
     * Libera y destruye totalmente la sesion y sus variables.
     */
    public function destruir()
    {
        if (session_status() === PHP_SESSION_ACTIVE) {
            if (ini_get('session.use_cookies')) {
                $params = session_get_cookie_params();
                setcookie(session_name(), '', time() - 42000,
                    $params['path'], $params['domain'],
                    $params['secure'], $params['httponly']
                );
            }
            session_unset();
            session_destroy();
        }
    }

    /**
     * Define anuncio importante para el usuario.
     *
     * @param string $mensaje
     * @param string $tipo
     */
    public function flash($mensaje, $tipo = 'error')
    {
        if (session_status() !== PHP_SESSION_ACTIVE) $this->iniciar();

        $_SESSION['__flash']['mensaje'] = $mensaje;
        $_SESSION['__flash']['tipo'] = $tipo;
    }

    /**
     * Obtiene anuncio importante para el usuario.
     *
     * @return array|null
     */
    public function obtFlash()
    {
        if (session_status() !== PHP_SESSION_ACTIVE) $this->iniciar();

        if (isset($_SESSION['__flash']['mensaje'])) {
            return$this->eliminar('__flash');
        } else {
            return null;
        }
    }

    /**
     * Elimina anuncio importante.
     *
     * @return mixed
     */
    public function eliminarFlash()
    {
        return $this->eliminar('__flash');
    }

    /**
     * Obtiene una variable de sesion.
     *
     * @param string $nombre
     * @param mixed $valorPredeterminado
     * @return mixed
     */
    public function obtener($nombre, $valorPredeterminado = null)
    {
        if (session_status() !== PHP_SESSION_ACTIVE) $this->iniciar();

        return isset($_SESSION[$nombre]) ? $_SESSION[$nombre] : $valorPredeterminado;
    }

    /**
     * Guarda una variable de session.
     *
     * @param string $nombre Nombre
     * @param mixed $valor Valor
     */
    public function guardar($nombre, $valor)
    {
        if (session_status() !== PHP_SESSION_ACTIVE) $this->iniciar();

        $_SESSION[$nombre] = $valor;
    }

    /**
     * Elimina una variable de sesion.
     *
     * @param string $nombre
     * @return mixed el valor de la variable eliminada o nulo si no existe variable
     */
    public function eliminar($nombre)
    {
        if (session_status() !== PHP_SESSION_ACTIVE) $this->iniciar();

        if (isset($_SESSION[$nombre])) {
            $valor = $_SESSION[$nombre];
            unset($_SESSION[$nombre]);
            return $valor;
        } else {
            return null;
        }
    }

    /**
     * Elimina todas las variables de sesion.
     */
    public function limpiar()
    {
        if (session_status() !== PHP_SESSION_ACTIVE) $this->iniciar();

        session_unset();
    }

    /**
     * Verifica si existe una variable de sesion.
     *
     * @param string $nombre
     * @return boolean
     */
    public function existe($nombre)
    {
        if (session_status() !== PHP_SESSION_ACTIVE) $this->iniciar();

        return isset($_SESSION[$nombre]);
    }

    /**
     * Actualiza el id de sesión actual con uno generado más reciente.
     *
     * @param boolean $borrar_anterior
     * @return boolean
     */
    public function regenerarId($borrar_anterior = true)
    {
        if (session_status() !== PHP_SESSION_ACTIVE) $this->iniciar();

        return session_regenerate_id($borrar_anterior);
    }

    /**
     * Obtiene el id de sesión actual.
     *
     * @return string
     */
    public function obtenerId()
    {
        if (session_status() !== PHP_SESSION_ACTIVE) $this->iniciar();

        return session_id();
    }

    private function obtenerIp()
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        if (isset($_SERVER['REMOTE_ADDR'])) {
            return $_SERVER['REMOTE_ADDR'];
        }
        return '(unknown)';
    }
}
