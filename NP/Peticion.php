<?php

namespace NP;

/**
 * Procesador de petición http
 *
 * @author Néstor Picado <info@nestorpicado.com>
 * @package NP
 * @version 0.6 (2014-08-05)
 */
class Peticion
{
    private $parametros;

    /**
     * Detecta si la petición se hizo a traves de ajax
     *
     * @return bool
     */
    public function esAjax()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Detecta si la petición es segura
     *
     * @return bool
     */
    public function esSegura()
    {
        if (!empty($_SERVER['HTTPS']) || $_SERVER['SERVER_PORT'] == 443) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Devuelve la base de la petición
     *
     * @return string
     */
    public function obtenerBase()
    {
        $esquema = $this->obtenerEsquema();

        if (isset($_SERVER['HTTP_HOST'])) {
            return $esquema . '://' . $_SERVER['HTTP_HOST'];
        } else {
            return $esquema . '://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'];
        }
    }

    /**
     * Devuelve el puerto de la petición
     *
     * @return string
     */
    public function obtenerPuerto()
    {
        return $_SERVER['SERVER_PORT'];
    }

    /**
     * Devuelve el ancla de la petición
     *
     * @return string
     */
    public function obtenerAncla()
    {
        $temp = parse_url($_SERVER['REQUEST_URI']);

        if (isset($temp['fragment'])) {
            return $temp['fragment'];
        } else {
            return null;
        }
    }

    /**
     * Devuelve el query de la petición
     *
     * @return string
     */
    public function obtenerQuery()
    {
        $temp = parse_url($_SERVER['REQUEST_URI']);

        if (isset($temp['query'])) {
            return $temp['query'];
        } else {
            return null;
        }
    }

    /**
     * Devuelve el camino de la petición
     *
     * @return string
     */
    public function obtenerCamino()
    {
        $temp = parse_url($_SERVER['REQUEST_URI']);

        return $temp['path'];
    }

    /**
     * Devuelve el uri de la petición
     *
     * @return string
     */
    public function obtenerUri()
    {
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * Devuelve el esquema de la petición
     *
     * @return string el esquema de la petición
     */
    public function obtenerEsquema()
    {
        if (isset($_SERVER['REQUEST_SCHEME'])) {
            return $_SERVER['REQUEST_SCHEME'];
        } elseif (!empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'off')) {
            return 'https';
        } else {
            return 'http';
        }
    }

    /**
     * Devuelve el host de la petición
     *
     * @return string el host de la petición
     */
    public function obtenerHost()
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            $temp = $_SERVER['HTTP_HOST'];
        } else {
            $temp = $_SERVER['SERVER_NAME'];
        }

        $temp = explode(':', $temp);

        return $temp[0];
    }

    /**
     * Devuelve el método de la petición
     *
     * @return string el método de la petición en minuscula
     */
    public function obtenerMetodo()
    {
        if (isset($_SERVER['REQUEST_METHOD'])) {
            return strtolower($_SERVER['REQUEST_METHOD']);
        } else {
            return 'get';
        }
    }

    /**
     * Devuelve el valor del parámetro GET llamado.
     * Si el parámetro GET no existe, se devolverá el segundo parámetro de este método.
     *
     * @param string $nombre Nombre del parámetro
     * @param mixed $por_defecto El valor del parámetro por defecto
     * @return mixed El valor del parámetro GET
     * @see varPost
     */
    public function obtGet($nombre, $por_defecto = null)
    {
        return isset($_GET[$nombre]) ? $_GET[$nombre] : $por_defecto;
    }

    /**
     * Devuelve el valor del parámetro POST llamado.
     * Si el parámetro POST no existe, se devolverá el segundo parámetro de este método.
     *
     * @param string $nombre Nombre del parámetro
     * @param mixed $por_defecto El valor del parámetro por defecto
     * @return mixed El valor del parámetro POST
     * @see varGet
     */
    public function obtPost($nombre, $por_defecto = null)
    {
        return isset($_POST[$nombre]) ? $_POST[$nombre] : $por_defecto;
    }

    /**
     * Devuelve el valor del parámetro dentro de la peticion HTTP.
     * Si el parámetro no existe, se devolverá el segundo parámetro de este método.
     *
     * @param string $nombre Nombre del parámetro
     * @param mixed $por_defecto El valor del parámetro por defecto
     * @return mixed El valor del parámetro
     * @see varGet
     * @see varPost
     */
    public function obtParam($nombre, $por_defecto = null)
    {
        if ($this->parametros === null) {
            $temp = file_get_contents('php://input');
            if (function_exists('mb_parse_str'))
                mb_parse_str($temp, $this->parametros);
            else
                parse_str($temp, $this->parametros);
        }

        return isset($this->parametros[$nombre]) ? $this->parametros[$nombre] : $por_defecto;
    }

    /**
     * Devuelve información acerca de las capacidades del navegador del usuario.
     * @return array
     */
    public function obtNavegador()
    {
        return get_browser(null, true);
    }

    /**
     * Devuelve IP del usuario.
     * @return array
     */
    public function obtenerIP()
    {
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        }

        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Envia un archivo al usuario.
     *
     * @param string $nombre
     * @param string $contenido
     * @param string $mimeType
     * @param boolean $terminar
     */
    public function enviarArchivo($nombre, $contenido, $mimeType = 'text/plain', $terminar = true)
    {
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header("Content-type: $mimeType");
        header('Content-Length: ' . (function_exists('mb_strlen') ? mb_strlen($contenido, '8bit') : strlen($contenido)));
        header("Content-Disposition: attachment; filename=\"$nombre\"");
        header('Content-Transfer-Encoding: binary');

        if ($terminar) {
            echo $contenido;
            exit(0);
        } else
            echo $contenido;
    }

    /**
     * Detecta el idioma del navegador del usuario.
     *
     * @param string $por_defecto
     * @return string
     */
    public function detectarIdioma($por_defecto = 'es')
    {
        if (!($idioma = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']))) {
            $idioma = $por_defecto;
        }

        return $idioma;
    }

    /**
     * Redirige el navegador a la URL especificada.
     *
     * @param string $url
     * @param integer $codigo
     */
    public function redirigir($url, $codigo = 302)
    {
        header('Location: ' . $url, true, $codigo);
        exit;
    }
}
