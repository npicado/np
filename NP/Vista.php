<?php

namespace NP;

/**
 * Capa Vista
 *
 * @author Néstor Picado <info@nestorpicado.com>
 * @package NP
 * @version 0.2 (2014-06-24)
 */
class Vista extends \stdClass
{
    /**
     * @var Servicios
     */
    protected $s;
    protected $contenido;
    public $plantilla;

    public function __construct(Servicios $s)
    {
        $this->s = $s;
    }

    private function obtenerContenido($archivo)
    {
        $archivo = $this->s['config']['app_dir'] . '/vistas/' . $archivo . '.php';
        if (file_exists($archivo)) {
            ob_start();
            include $archivo;
            return ob_get_clean();
        } else {
            throw new Excepcion('La vista "' . $archivo . '" no se pudo incluir porque no existe.');
        }
    }

    private function procesarValor($procesador, $valor)
    {
        if (strpos($procesador, '+') !== false) {
            $procesadores = explode('+', $procesador);
            foreach ($procesadores as $p) {
                $valor = $this->procesarValor($p, $valor);
            }
        } else {
            if ($procesador == 'e') {
                $valor = htmlspecialchars($valor, ENT_QUOTES | ENT_IGNORE | ENT_HTML5);
            } elseif ($procesador == 't') {
                $valor =  mb_convert_case($valor, MB_CASE_TITLE);
                $palabras_omitidas = array(
                    ' de ', ' un ', ' una ', ' uno ', ' el ', ' la ', ' lo ', ' las ', ' los ', ' de ', ' y ',
                    ' o ', ' ni ', ' pero ', ' es ', ' e ', ' si ', ' entonces ', ' sino ', ' cuando ', ' al ',
                    ' desde ', ' por ', ' para ', ' en ', ' off ', ' dentro ', ' afuera ', ' sobre ', ' a ',
                    ' adentro ', ' con ', ' su ');
                $valor = trim(str_ireplace($palabras_omitidas, $palabras_omitidas, $valor . ' '));
            } elseif ($procesador == 'u') {
                $valor = mb_convert_case($valor, MB_CASE_UPPER);
            } elseif ($procesador == 'l') {
                $valor = mb_convert_case($valor, MB_CASE_LOWER);
            } elseif ($procesador == 'tc') {
                $valor = mb_convert_case($valor, MB_CASE_TITLE);
            }
        }

        return $valor;
    }

    /**
     * Procesa y muestra una vista con códigos php y etiquetas propias
     *
     * @param string $vista
     * @param bool $imprimir
     * @return string
     */
    public function renderizar($vista, $imprimir = true)
    {
        $this->contenido = $this->obtenerContenido($vista);

        if (isset($this->plantilla)) {
            $this->contenido = $this->obtenerContenido('plantillas/' . $this->plantilla);
        }

        if (strpos($this->contenido, '{{') !== false) {

            preg_match_all('#\{\{([^\{\}]+)\}\}#u', $this->contenido, $ocurrencias);

            foreach ($ocurrencias[1] as $ocurrencia) {

                // Extraemos procesador
                $procesador = null;
                $temp = explode('|', $ocurrencia);
                if (isset($temp[1])) {
                    $procesador = $temp[0];
                    $valor = $temp[1];
                } else {
                    $valor = $temp[0];
                }

                // Extraemos fuente de datos
                $fuente = null;
                $temp = explode(':', $valor);
                if (isset($temp[1])) {
                    $fuente = $temp[0];
                    $valor = $temp[1];
                } else {
                    $valor = $temp[0];
                }

                // Obtenemos valor
                if ($fuente) {
                    if ($fuente == 'm') {
//                        $valor = Mensajes::t($valor);
                        // TODO: Integar con Gettext
                    }
                } else {
                    if (isset($this->{$valor})) {
                        $valor = $this->{$valor};
                    }
                }

                // Procesamos valor en caso necesario
                if ($procesador) {
                    $valor = $this->procesarValor($procesador, $valor);
                }

                $this->contenido = str_replace('{{'.$ocurrencia.'}}', $valor, $this->contenido);
            }
        }

        if ($imprimir) {
            echo $this->contenido;
        }
        return $this->contenido;
    }

    public function importar(array &$variables)
    {
        foreach ($variables as $llave => $valor) {
            $this->{$llave} = $valor;
        }
    }

    /**
     * @return Peticion
     */
    public function peticion()
    {
        return $this->s['peticion'];
    }

    /**
     * @return Sesion
     */
    public function sesion()
    {
        return $this->s['sesion'];
    }

}
