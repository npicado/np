<?php

namespace NP;

/**
 * Aplicacion Web
 *
 * @package NP
 * @author Néstor Picado <info@nestorpicado.com>
 * @version 0.5 (2015-02-15)
 */
class Aplicacion
{
    /**
     * @var Servicios
     */
    private $s;
    private $rutas;

    public function __construct(Servicios $servicios)
    {
        $this->s = $servicios;
    }


    /**
     * Corre una ruta mixta
     *
     * @param $ruta
     * @return mixed
     * @throws Excepcion
     */
    private function correrRuta($ruta)
    {
        if (is_array($ruta)) {

            // Corremos ruta de redireccion en caso necesario
            if ($ruta[0] == 'redir') {

                if (isset($ruta[2])) {
                    $cod = $ruta[2];
                } else {
                    $cod = 302;
                }

                header('Location: ' . $ruta[1], true, $cod);
                return null;

            } else {

                // Ubicamos e incluimos archivo que contiene el controlador
                $archivo = $this->s['config']['app_dir'] . '/controladores/' . str_replace('\\', '/', $ruta[0])  . '.php';
                if (!is_file($archivo)) {
                    throw new Excepcion('El controlador "' . $ruta[0] . '" no existe.');
                }
                require_once $archivo;

                // Instanciamos el controlador
                $controlador_nombre = $ruta[0] . 'Controlador';
                $controlador = new $controlador_nombre($this->s);

                // Traspasamos parametros de la ruta al controlador
                if (isset($ruta[2])) {
                    $controlador->parametros = $ruta[2];
                }

                // Ejecutamos evento de inicio en el controlador
                if ($temp = $controlador->alIniciar($ruta[0], $ruta[1])) {
                    return $this->correrRuta($temp);
                }

                // Detectamos presencia de accion y la ejecutamos si la encontramos
                if (method_exists($controlador, $ruta[1])) {
                    $temp = $controlador->$ruta[1]();
                } else {
                    throw new Excepcion('La acción "' . $ruta[1] . '" no existe dentro del controlador.');
                }

                // Ejecutamos evento de terminacion en el controlador
                $controlador->alTerminar($temp);

                if ($temp) {
                    return $this->correrRuta($temp);
                } else {
                    return null;
                }

            }

        } elseif (is_int($ruta)) {

            http_response_code($ruta);

            if (isset($this->rutas[$ruta])) {
                return $this->correrRuta($this->rutas[$ruta]);
            }

            return null;

        } elseif (is_string($ruta)) {

            $vista = new Vista($this->s);
            $vista->renderizar($ruta);

            return null;

        }

        // Prohibimos acceso en caso de ruta anormal
//        header('HTTP/1.0 403 Forbidden');
        http_response_code(403);
        echo 'Acceso prohibido!';
        return null;
    }

    public function ejecutar()
    {
        header('X-Powered-By: NP', true);

        // Definimos zona horaria segun configuración
        if (isset($this->s['config']['app_zonatiempo'])) {
            date_default_timezone_set($this->s['config']['app_zonatiempo']);
        }

        // Definimos tipo de respuesta segun configuración
        if (isset($this->s['config']['app_tipo_respuesta'])) {
            header('Content-Type: ' . $this->s['config']['app_tipo_respuesta']);
        }

        // Definimos codificación textual segun configuración
        if (isset($this->s['config']['app_codificacion'])) {
            mb_internal_encoding($this->s['config']['app_codificacion']);
            if ($this->s['config']['app_codificacion'] == 'UTF-8') {
                mb_detect_order('UTF-8,ISO-8859-1');
            }
        }

        // Cargamos las rutas a la aplicacion
        $this->rutas = require $this->s['config']['app_dir'] . '/config/rutas.php';

        // Ejecutamos enrutador para obtener la ruta a correr
        include 'Enrutador.php';
        $enrutador = new Enrutador();
        $ruta = $enrutador->ejecutar($this->rutas, $this->s['config']['app_uri_base']);

        // ['XXX', 'XXX', [XXX]] -> Se ejecuta como una ruta normal
        // ['redir', 'XXX', XXX] -> Se ejecuta como una redirección manual
        // 'XXXX' -> Se ejecuta como vista.
        // 000 -> Se ejecuta como códigos de estado HTTP

        $this->correrRuta($ruta);
    }
}
