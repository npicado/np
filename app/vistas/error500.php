<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Error 500 :(</title>
    <style>
        ::-moz-selection {
            background: #b3d4fc;
            text-shadow: none;
        }

        ::selection {
            background: #b3d4fc;
            text-shadow: none;
        }

        html,
        input {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        body {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            line-height: 1.4;
            color: #737373;
            background: #f0f0f0;
        }

        .contenedor {
            margin: 50px auto;
            padding: 30px 30px 50px;
            max-width: 720px;
            border: 1px solid #b3b3b3;
            border-radius: 5px;
            box-shadow: 0 1px 10px #a7a7a7, inset 0 1px 0 #fff;
            background: #fcfcfc;
        }

        h1 {
            margin: 0 0 30px 0;
            font-size: 30px;
            line-height: 1.6;
            color: #000;
        }

        h1 span {
            color: #bbb;
        }

        h3 {
            margin: 1.5em 0 0.5em;
        }

        p {
            margin: 1em 0;
        }

        ul {
            padding: 0 0 0 30px;
            margin: 1em 0;
        }
        li {
            margin: 0.4em 0;

        }
    </style>
</head>
<body>
<div class="contenedor">
    <h1>Lo sentimos, hubo un problema interno sirviendo la página solicitada.</h1>
    <p>Ahora usted se estará preguntando, "¿Qué hago ahora?". Bueno... </p>
    <ul>
        <li>puede tratar de refrescar la página, el problema puede ser temporal.</li>
        <li>si ha introducido la dirección URL a mano, vuelva a comprobar que es correcta.</li>
    </ul>

    <p>Hemos sido notificados del problema y haremos todo lo posible para asegurar que no vuelva a suceder!</p>
</div>
</body>
</html>
