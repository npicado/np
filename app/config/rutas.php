<?php

// PRUEBA
$rutas['*']['/ejemplo'] = ['Ejemplos', 'prueba', ['param1' => 1]];
$rutas['*']['/nada'] = ['redir', '/ejemplo', 302];
$rutas['*']['/ejemplo/{nombre}'] = ['Prueba', 'texto'];
$rutas['*']['/ejemplo/{nombre}/{apellido}/'] = ['Ejemplos', 'prueba2', ['prueba' => 1]];

// PORTADA
$rutas['*']['/'] = ['Portada', 'todo'];
$rutas['get']['/'] = ['Portada', 'get'];
$rutas['post']['/'] = ['Portada', 'post'];

$rutas[500] = ['Errores', 'error500'];
$rutas[404] = ['Errores', 'error404'];
$rutas[404] = 'error404';


return $rutas;
