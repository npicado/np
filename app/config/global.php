<?php

$raiz = realpath(__DIR__ . '/../../');

return [
    'app_dir'               => $raiz . '/app',
    'app_uri_base'          => '/',
    'app_codificacion'      => 'UTF-8',
    'app_zonatiempo'        => 'America/Managua',
    'app_tipo_respuesta'    => 'application/json; charset=UTF-8',
];
